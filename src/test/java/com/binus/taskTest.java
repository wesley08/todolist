package com.binus;

import org.junit.Test;

import static org.junit.Assert.*;

public class taskTest {
    @Test
    public  void testAddTask(){
        String expectedValue = "1. NGODING [NOT_DONE]";

        task firstTask = new task(1, "NGODING", statusOfTask.NOT_DONE);

        assertEquals(firstTask,expectedValue);
    }
}