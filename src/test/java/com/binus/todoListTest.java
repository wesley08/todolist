package com.binus;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class todoListTest {
    @Test
    public void getAllTaskListMatchWithExpectedValue(){
        String expected = "1. Main [DONE]\n3. Belajar JAVA [NOT_DONE]\n2. TDD [DONE]";
        todoList todolist = new todoList();
        todolist.addTheTask(new task(1, "Main", statusOfTask.DONE));
        todolist.addTheTask(new task(3, "Belajar JAVA", statusOfTask.NOT_DONE));
        todolist.addTheTask(new task(2, "TDD", statusOfTask.DONE));
        assertEquals(expected, todolist.getAllOfTheTask());
    }

}