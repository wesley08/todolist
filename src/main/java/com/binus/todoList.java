package com.binus;

import java.util.ArrayList;

public class todoList {
    private ArrayList<task> taskOfList  = new ArrayList<>();

    public void addTheTask(task task){
    taskOfList.add(task);
    }

    boolean condition(int i , int lengthOfTheAllTask ){
       return i != lengthOfTheAllTask-1;
    }

    public String getAllOfTheTask(){
        int lengthOfTheAllTask = taskOfList.size();
        String tempOfTheTask = "";
        for (int i =0; i < lengthOfTheAllTask;i++){
            task task = taskOfList.get(i);
            String getAllDataOfTask = task.getIdOfTask()+". "+task.getNameOfTask()+" ["+task.getStatusOfTask()+"]";
            if(condition(i,lengthOfTheAllTask)) tempOfTheTask += getAllDataOfTask +"\n";
            else tempOfTheTask += getAllDataOfTask;
        }
        return tempOfTheTask;
    }

}
