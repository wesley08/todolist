package com.binus;

public class task {
    private int idOfTask;
    private String nameOfTask;
    private statusOfTask statusOfTask;

    public task(int id , String nameTask, statusOfTask statusTask){
        this.idOfTask = id;
        this.nameOfTask = nameTask;
        this.statusOfTask = statusTask;
    }

    public int getIdOfTask() {
        return idOfTask;
    }

    public void setIdOfTask(int idOfTask) {
        this.idOfTask = idOfTask;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    public statusOfTask getStatusOfTask() {
        return statusOfTask;
    }

    public void setStatusOfTask(statusOfTask statusOfTask) {
        this.statusOfTask = statusOfTask;
    }
}
